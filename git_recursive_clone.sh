#!/bin/bash -e

#############
# Variables #
#############
GIT_DIR="${HOME}/Documents/git"
GITLAB="gitlab.com"
TOKEN=''
QUIET=''

#############
# Functions #
#############
# Main function call
main(){
  parse_options "$@"
  get_projects_json
  PROJECTS=$(cat /tmp/gitlab_page*.json| \
    grep -o "\"path_with_namespace\":[^,]\+"| \
    tr '"' ' '|awk '{print $NF}')
  make_group_directories
  clone_or_fetch
  clean_temporary_files
}

# Get the command line arguments
parse_options(){
  if [ "$#" -lt 1 ]
 then
    print_usage
    exit 1
  fi

  while getopts "d:g:t:hq" option
  do
    case $option in
      d)
        GIT_DIR=$OPTARG
        ;;
      g)
        GITLAB=$OPTARG
        ;;
      t)
        TOKEN=$OPTARG
        ;;
      q)
        QUIET='-q'
        ;;
      h)
        print_usage
        exit 1
        ;;
      \?)
        echo "Invalid option: -$OPTARG." >&2
        print_usage
        exit 2
        ;;
      :)
        echo "Option -$OPTARG requires an argument." >&2
        print_usage
        exit 3
        ;;
    esac
  done

  if [[ "${TOKEN}" == "" ]]
  then
    echo "Token is a mandatory option."
    print_usage
    exit 4
  fi
}

# Print the usage.
print_usage(){
  echo "USAGE:" >&2
  echo "  $0 [options] -t TOKEN" >&2
  echo "    -d GITDIR   Directory where the git projects will be." >&2
  echo "                (Default: ${HOME}/Documents/git)" >&2
  echo "    -g GITLAB   Server name of the gitlab server." >&2
  echo "                May be a private repository." >&2
  echo "                (Default: gitlab.com)" >&2
  echo "    -t TOKEN    Personnal Access Token (Mandatory)." >&2
  echo "    -q          Quietly run the script."
  echo "    -h          Print this help." >&2
  echo "Don't forget to copy your ssh public key into gitlab." >&2
  echo "WARNING: Any non pushed changes will be stashed and then popped." >&2
  echo "         All commits will be hard reset to origin/master." >&2
  echo "" >&2
}

# Get all project pages.
get_projects_json(){
  wget -q --no-check-certificate --save-headers \
    "http://${GITLAB}/api/v3/projects/?private_token=${TOKEN}" \
    -O "/tmp/headers.json"
  PAGE_NUM=$(grep X-Total-Pages /tmp/headers.json|awk '{print $2}'| \
    sed 's/\s*//g')
  for PAGE in $(seq 1 ${PAGE_NUM})
  do
  wget -q --no-check-certificate \
    "http://${GITLAB}/api/v3/projects/?private_token=${TOKEN}&page=${PAGE}" \
    -O "/tmp/gitlab_page${PAGE}.json"
  done
}

# Create groups directories.
make_group_directories(){
  for group in $(echo $PROJECTS|cut -d/ -f1|sort|uniq)
  do
    echo "Create ${GIT_DIR}/${group}"
    mkdir -p $GIT_DIR/$group
  done
}

# Git clone or force fetch from origin.
clone_or_fetch(){
  for project in $(echo "${PROJECTS}"); do
    if [ -e "${GIT_DIR}/${project}/" ]
    then
      echo "Fetch ${project}"
      pushd "${GIT_DIR}/${project}"
      git stash $QUIET
      git checkout master $QUIET
      git fetch --all $QUIET
      git reset --hard origin/master $QUIET
      git stash pop $QUIET
      popd
    else
      echo "Clone ${GITLAB}/${project}.git"
      git clone "git@${GITLAB}:${project}.git" "${GIT_DIR}/${project}" $QUIET
    fi
  done
}

# Clean temporary files in /tmp directory.
clean_temporary_files(){
  rm /tmp/{headers,gitlab_page*}.json
}

########
# Main #
########
main $@

exit 0
